<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostsController@index');
Auth::routes();

Route::group(['middleWare' => 'auth'], function () {
    Route::get('/home', 'PostsController@index')->name('home');
    Route::resource('posts', 'PostsController');
    Route::resource('comments', 'CommentsController');
    Route::get('userPosts', 'PostsController@userPosts');
});