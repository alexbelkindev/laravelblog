@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="blog-header">
            <div class="panel panel-body">
                <h1 class="blog-title">Simple Blog</h1>
                <p class="lead blog-description">Powered by Laravel 5.5.21</p>
            </div>
        </div>
        @yield('posts')
    </div>
@endsection