@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Comments</div>
                    <div class="panel-body">
                        @if($comments->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Comment ID</th>
                                    <th>Post ID</th>
                                    <th>Text</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($comments as $comment)
                                    <tr>
                                        <td>{{$comment->id}}</td>
                                        <td>{{$comment->post_id}}</td>
                                        <td>{{$comment->body}}</td>
                                        <td>
                                            <form method="post" action="{{route('comments.destroy', $post->id)}}">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="text-center">
                                {{$comments->links()}}
                            </div>
                        @else
                            You have no comments yet.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
