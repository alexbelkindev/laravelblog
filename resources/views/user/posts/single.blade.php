@extends('welcome')

@section('posts')
    <div class="panel panel-body">
        <div class="blog-main">
            @if($post->count() > 0)
                <div class="blog-post">
                    <h2 class="blog-post-title">{{$post->title}}</h2>
                    <p class="blog-post-meta">
                        Author: {{\App\User::find($post->user_id)->name}}
                    </p>
                    <p>{{$post->body}}</p>
                </div>
            @else
                <p>No posts found. Please log in and create some.</p>
            @endif
        </div>
    </div>
    <div class="panel panel-body">
        <div class="comments-list">
            <h3 class="panel-heading text-center">Comments</h3>
            @if($comments->count() > 0)
                @foreach($comments as $comment)
                    <h4 class="media-heading user_name">{{\App\User::find($comment->user_id)->name}}</h4>
                    <p>{{$comment->body}}</p>
                    <hr>
                @endforeach
                <div class="text-center">
                    {{$comments->links()}}
                </div>
            @else
                <p>Post has no comments.</p>
            @endif
        </div>
        @guest
        <p>Please sign up or log in to be able write comments.</p>
        @else
            {!! Form::open(['route' => 'comments.store', 'class' => 'form']) !!}

            {!! Form::hidden('post_id', $post->id ) !!}

            <div class="form-group">
                {!! Form::label('Comment') !!}
                {!! Form::textarea('body', null,
                    array('required',
                          'class'=>'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Add comment',
                  array('class'=>'btn btn-primary')) !!}
            </div>
            {!! Form::close() !!}
            @endguest
    </div>
@endsection