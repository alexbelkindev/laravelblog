@extends('welcome')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create post</div>
                    <div class="panel-body">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>

                        {!! Form::open(array('route' => 'posts.store', 'class' => 'form')) !!}

                        <div class="form-group">
                            {!! Form::label('Title') !!}
                            {!! Form::text('title', null,
                                array('required',
                                      'class'=>'form-control',
                                      'placeholder'=>'Post title')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('Body') !!}
                            {!! Form::textarea('body', null,
                                array('required',
                                      'class'=>'form-control',
                                      'placeholder' => 'Post text')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Add post',
                              array('class'=>'btn btn-primary')) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection