@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">My posts</div>
                    <div class="panel-body">
                        @if($posts->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Post ID</th>
                                    <th>Title</th>
                                    <th>Text</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($posts as $post)
                                    <tr>
                                        <td>{{$post->id}}</td>
                                        <td>{{$post->title}}</td>
                                        <td>{{$post->body}}</td>
                                        <td>
                                            <a class="btn btn-danger" type="button"
                                               href="{{route('comments.destroy', $post->id)}}">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="text-center">
                                {{$posts->links()}}
                            </div>
                        @else
                            You have no comments yet.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
