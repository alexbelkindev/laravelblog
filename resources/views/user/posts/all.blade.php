@extends('welcome')

@section('posts')
    <div class="panel panel-body">
        <div class="blog-main">
            @if($posts->total() > 0)
                @foreach($posts as $post)
                    <div class="blog-post">
                        <a href="{{route('posts.show', $post->id)}}">
                            <h2 class="blog-post-title">{{$post->title}}</h2>
                        </a>
                        <p class="blog-post-meta">
                            Author: {{\App\User::find($post->user_id)->name}}
                        </p>
                        <p>{{$post->body}}</p>
                    </div>
                    <hr>
                @endforeach
                <div class="text-center">
                    {{$posts->links()}}
                </div>
            @else
                <p>No posts found. Please log in and create some.</p>
            @endif
        </div>
    </div>
@endsection