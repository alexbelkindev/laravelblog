<?php
/**
 * Created by PhpStorm.
 * User: alexbelkin
 * Date: 26.11.2017
 * Time: 0:20
 */

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->text(),
        'user_id' => factory(App\User::class)->create()->id,
        'post_id' => factory(App\Post::class)->create()->id,
    ];
});