<?php
/**
 * Created by PhpStorm.
 * User: alexbelkin
 * Date: 26.11.2017
 * Time: 0:20
 */

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->title(),
        'body' => $faker->paragraphs(5),
        'user_id' => factory(App\User::class)->create()->id,
    ];
});