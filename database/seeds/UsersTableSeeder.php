<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Comment;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()->each(function ($user) {
            $user->comments()->saveMany(factory(Comment::class, 5)->create(['user_id' => $user->id]));
        });
    }
}
